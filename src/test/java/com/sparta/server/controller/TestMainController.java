package com.sparta.server.controller;

import com.sparta.server.model.Record;
import com.sparta.server.service.RecordService;
import com.sparta.server.service.RecordServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@Slf4j
@WebMvcTest(MainController.class)
class TestMainController {

    @TestConfiguration
    protected static class TestConfig {
        @Bean
        public RecordService recordService() {
            return Mockito.mock(RecordServiceImpl.class);
        }
    }

    @Mock
    RecordService recordService;

    MainController mainController;

    @BeforeEach
    private void setUp() throws IOException {
        this.mainController = new MainController(recordService);

        int numberOfRecords = 29;
        List<Record> resultListMock = mock(List.class);
        doReturn(numberOfRecords).when(resultListMock).size();
        doReturn(resultListMock).when(recordService)
                .deserializeBatchOfRecords(ArgumentMatchers.<DataInputStream>any(), anyLong());
    }

    @Test
    public void shouldReturnNumberOfRecordsSent() throws Exception {
        byte[] bytes = getTestData("testData29Records.bin");
        long expectedNumberOfRecords = 29;

        long actualNumberOfRecords = mainController.load("aaa", bytes);
        assertThat(actualNumberOfRecords).isEqualTo(expectedNumberOfRecords);
    }

    private byte[] getTestData(String filePath) throws IOException, URISyntaxException {
        return Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(filePath).toURI()));
    }
}

package com.sparta.server.service;

import com.sparta.server.model.Record;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class RecordStoreImpl implements RecordStore {

    private HashMap<String, List<Record>> store = new HashMap<>();

    public Optional<List<Record>> getRecords(String provider) {
        return Optional.ofNullable(store.get(provider));
    }

    public void putRecords(String provider, List<Record> records) {
        store.computeIfAbsent(provider, k -> new ArrayList<>()).addAll(records);
    }
}

package com.sparta.server.service;

import com.sparta.server.model.Record;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

public interface RecordService {

    /**
     * Deserialize input stream to a list of records
     *
     * @param dis the DataInputStream to read from
     * @param numberOfRecords
     * @return
     * @throws IOException
     */
    List<Record> deserializeBatchOfRecords(DataInputStream dis, long numberOfRecords) throws IOException;

    /**
     * Saves the records belonging to a provider in a store
     *
     * @param provider name of the provider
     * @param records a list of records
     */
    void saveRecords(String provider, List<Record> records);

    /**
     * Returns the total number of records for provider
     *
     * @param provider name of the provider
     * @return
     */
    int getTotalRecords(String provider);

}

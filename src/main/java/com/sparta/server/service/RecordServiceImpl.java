package com.sparta.server.service;

import com.sparta.server.model.Record;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class RecordServiceImpl implements RecordService {

    private RecordStore recordStore;

    public RecordServiceImpl(RecordStore recordStore) {
        this.recordStore = recordStore;
    }

    public List<Record> deserializeBatchOfRecords(DataInputStream dis) throws IOException {
        log.info("Deserializing batch of records.");
        
        long numberOfRecords = dis.readLong();
        log.info("numberOfRecords: " + numberOfRecords);
        
        List<Record> records = new ArrayList<>();
        for (int i = 0; i < numberOfRecords; i++) {
            records.add(new Record(dis));
        }
        dis.close();

        return records;
    }

    @Override
    public void saveRecords(String provider, List<Record> records) {
        log.info("Storing " + records.size() + " records to provider " + provider);
        recordStore.putRecords(provider, records);
    }

    @Override
    public int getTotalRecords(String provider) {
        return recordStore.getRecords(provider).orElse(Collections.emptyList()).size();
    }
}

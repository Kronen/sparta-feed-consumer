package com.sparta.server.service;

import com.sparta.server.model.Record;

import java.util.List;
import java.util.Optional;

public interface RecordStore {

    Optional<List<Record>> getRecords(String provider);

    void putRecords(String provider, List<Record> records);

}

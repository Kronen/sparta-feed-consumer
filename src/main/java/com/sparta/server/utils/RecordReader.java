package com.sparta.server.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.zip.CRC32;

@Slf4j
public class RecordReader {

    /**
     * Read the size of the string as an int from the input stream, then reads and returns the string.
     * @param dis the DataInputStream to read from
     * @return the String from the input stream
     * @throws IOException
     */
    public static String readString(final DataInputStream dis) throws IOException {
        int length = dis.readInt();
        byte[] array = new byte[length];
        dis.read(array);

        return new String(array, "UTF-8");
    }


    /**
     * Read {@code n} bytes from a {@code DataArrayInputStream} and returns its {@code CRC32} checksum,
     * returning the cursor to its original position before reading from the input stream.
     *
     * The DataInputStream must support mark/reset operations (e.g. {@code ByteArrayInputStream})
     *
     * @param dis the DataInputStream to read from
     * @param bytesLength the number of bytes to read
     * @return the CRC32 checksum as long
     * @throws IOException
     */
    public static long readChecksum(DataInputStream dis, int bytesLength) throws IOException {
        byte[] sensorArray = new byte[bytesLength];

        if(!dis.markSupported()) {
            throw new UnsupportedOperationException("The DataInputStream must support mark/reset operations");
        }
        dis.mark(bytesLength);
        dis.readFully(sensorArray, 0, bytesLength);
        dis.reset();

        CRC32 crc32 = new CRC32();
        crc32.update(sensorArray);

        return crc32.getValue();
    }
}

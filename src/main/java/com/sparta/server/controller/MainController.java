package com.sparta.server.controller;

import com.sparta.server.model.Record;
import com.sparta.server.service.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
public class MainController {

    RecordService recordService;

    public MainController(RecordService recordService) {
        this.recordService = recordService;
    }

    @PostMapping("/load/{provider}")
    public long load(@PathVariable("provider") String provider, @RequestBody byte[] content) throws IOException {
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(content));
        
        List<Record> records = recordService.deserializeBatchOfRecords(dis);
        recordService.saveRecords(provider, records);

        return records.size();
    }

    @GetMapping("/data/{provider}/total")
    public int total(@PathVariable("provider") String provider) {
        return recordService.getTotalRecords(provider);
    }

}

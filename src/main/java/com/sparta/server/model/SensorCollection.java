package com.sparta.server.model;

import com.sparta.server.utils.RecordReader;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Data
@Slf4j
public class SensorCollection {

    List<Sensor> sensors;
    long checksum;

    public SensorCollection(@NonNull DataInputStream dis) throws IOException {
        int numberBytesSensorData = dis.readInt();
        int numberOfSensors = dis.readInt();
        log.debug("numberOfSensors: " + numberOfSensors);
                
        this.checksum = RecordReader.readChecksum(dis, numberBytesSensorData);       

        this.sensors = new ArrayList<>(numberOfSensors);
        for(int i = 0; i < numberOfSensors; i++) {
            this.sensors.add(new Sensor(dis));
        }
    }
}

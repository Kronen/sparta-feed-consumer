package com.sparta.server.model;

import com.sparta.server.utils.RecordReader;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.IOException;

@Slf4j
public class Sensor {

    String id;
    int measure;

    public Sensor(@NonNull DataInputStream dis) throws IOException {
        this.id = RecordReader.readString(dis);
        this.measure = dis.readInt();
    }
}

package com.sparta.server.model;

import com.sparta.server.utils.RecordReader;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.IOException;

@Data
@Slf4j
public class Record {

    long recordIndex;
    long timestamp;
    String city;
    SensorCollection sensorsData;

    /**
     *  crc32 of all bytes present in the sensorsData section
     */
    long crc32SensorsData;

    public Record(@NonNull DataInputStream dis) throws IOException {
        this.recordIndex = dis.readLong();
        this.timestamp = dis.readLong();
        this.city = RecordReader.readString(dis);
        this.sensorsData = new SensorCollection(dis);
        this.crc32SensorsData = dis.readLong();
        
        if(this.crc32SensorsData != this.sensorsData.getChecksum()) {
            throw new IOException("Calculated CRC32 does not match the one read from the input. "
                    + "Stream might be corrupted.");
        }
    }

}
